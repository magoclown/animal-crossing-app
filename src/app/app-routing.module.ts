import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import { ItemFormComponent } from './layout/forms/item-form/item-form.component';
import { SubcategoryPageComponent } from './pages/subcategory-page/subcategory-page.component';
import { CategoryPageComponent } from './pages/category-page/category-page.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: IndexPageComponent,
      },
      {
        path: 'item/create',
        component: ItemFormComponent,
      },
      {
        path: 'subcategory/:id',
        component: SubcategoryPageComponent,
      },
      {
        path: 'category/:id',
        component: CategoryPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
