import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';
import { CategoryService } from './services/category.service';
import { FashionService } from './services/fashion.service';
import { ItemService } from './services/item.service';
import { SellerService } from './services/seller.service';
import { SubcategoryService } from './services/subcategory.service';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, PagesModule, HttpClientModule],
  providers: [
    CategoryService,
    FashionService,
    ItemService,
    SellerService,
    SubcategoryService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
