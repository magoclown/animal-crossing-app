import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';
import { CategoryService } from 'src/app/services/category.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.scss']
})
export class CategoryPageComponent implements OnInit {

  title: string;

  items: Item[];

  constructor(
    private itemService: ItemService,
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.itemService.getItemsWithAllByCategory(id).subscribe((items) => {
      this.items = items;
    });
    this.categoryService.getCategoriesById(id).subscribe((category) => {
      this.title = category.name;
    });
  }

}
