import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';

@Component({
  selector: 'app-subcategory-page',
  templateUrl: './subcategory-page.component.html',
  styleUrls: ['./subcategory-page.component.scss'],
})
export class SubcategoryPageComponent implements OnInit {

  title: string;

  items: Item[];

  constructor(
    private itemService: ItemService,
    private subcategoryService: SubcategoryService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));
    console.log(id);
    this.itemService.getItemsWithAllBySubcategory(id).subscribe((items) => {
      this.items = items;
    });
    this.subcategoryService.getSubcategorysById(id).subscribe((subcategory) => {
      this.title = subcategory.name;
    });
  }
}
