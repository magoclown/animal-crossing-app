import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/services/item.service';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss'],
})
export class IndexPageComponent implements OnInit {
  items: Item[];

  constructor(private itemService: ItemService) {}

  ngOnInit(): void {
    this.itemService.getItemsWithAll().subscribe((items) => {
      this.items = items;
    });
  }
}
