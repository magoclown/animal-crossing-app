import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { __exportStar } from 'tslib';

import { MainComponent } from './main/main.component';
import { LayoutModule } from '../layout/layout.module';
import { ItemFormPageComponent } from './item-form-page/item-form-page.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { AppRoutingModule } from '../app-routing.module';
import { SubcategoryPageComponent } from './subcategory-page/subcategory-page.component';
import { CategoryPageComponent } from './category-page/category-page.component';



@NgModule({
  declarations: [MainComponent, ItemFormPageComponent, IndexPageComponent, SubcategoryPageComponent, CategoryPageComponent],
  imports: [
    AppRoutingModule,
    CommonModule,
    LayoutModule
  ],
  exports: [
    MainComponent,
    ItemFormPageComponent,
    IndexPageComponent,
    SubcategoryPageComponent,
    CategoryPageComponent
  ]
})
export class PagesModule { }
