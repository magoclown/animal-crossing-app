import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { URLHelper } from '../helpers/url';
import { Subcategory } from '../models/subcategory.model';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {
  private static url = `${URLHelper.url}subcategories`;

  constructor(private http: HttpClient) {}

  getSubcategories(): Observable<Subcategory[]> {
    return this.http.get<Subcategory[]>(`${SubcategoryService.url}`);
  }

  getSubcategorysById(id: number): Observable<Subcategory> {
    return this.http.get<Subcategory>(`${SubcategoryService.url}/${id}`);
  }

  getSubcategorysByIdWithItems(id: number): Observable<Subcategory> {
    return this.http.get<Subcategory>(`${SubcategoryService.url}/wi/${id}`);
  }

  createSubcategory(Subcategory: Subcategory): Observable<Subcategory> {
    return this.http.post<Subcategory>(`${SubcategoryService.url}`, Subcategory);
  }

  updateSubcategory(Subcategory: Subcategory): Observable<Subcategory> {
    return this.http.put<Subcategory>(
      `${SubcategoryService.url}/${Subcategory.id}`,
      Subcategory
    );
  }
}
