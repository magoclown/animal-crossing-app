import { Injectable } from '@angular/core';

import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

import { S3Config } from '../config/s3.config';

@Injectable({
  providedIn: 'root',
})
export class UploadS3Service {
  constructor() {}

  async uploadFile(file: any, folder = '') {
    const contentType = file.type;
    const bucket = new S3({
      accessKeyId: S3Config.ACCESS_KEY_ID,
      secretAccessKey: S3Config.SECRET_ACCESS_KEY,
      region: S3Config.REGION,
    });
    const params: S3.PutObjectRequest = {
      Bucket: S3Config.BUCKET,
      Key: folder + '/' + file.name,
      Body: file,
      ACL: 'public-read',
      ContentType: contentType,
    };
    return new Promise(function (resolve, reject) {
      bucket.upload(params, function (err, data) {
        if (err) {
          console.error(`There was an error uploading your file: ${err}.`);
          reject(err)
        }
        console.log(`Succesfully upload file: ${data}`);

        resolve(data);
      });
    });
  }
}
