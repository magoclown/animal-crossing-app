import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { URLHelper } from '../helpers/url';
import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  private static url = `${URLHelper.url}items`;

  constructor(private http: HttpClient) {}

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(`${ItemService.url}`);
  }

  getItemsWithAll(): Observable<Item[]> {
    return this.http.get<Item[]>(`${ItemService.url}/wa`);
  }

  getItemsWithAllBySubcategory(id: number): Observable<Item[]> {
    return this.http.get<Item[]>(`${ItemService.url}/wa/sub/${id}`);
  }

  getItemsWithAllByCategory(id: number): Observable<Item[]> {
    return this.http.get<Item[]>(`${ItemService.url}/wa/cat/${id}`);
  }

  getItemsById(id: number): Observable<Item> {
    return this.http.get<Item>(`${ItemService.url}/${id}`);
  }

  createItem(Item: Item): Observable<Item> {
    return this.http.post<Item>(`${ItemService.url}`, Item);
  }

  updateItem(Item: Item): Observable<Item> {
    return this.http.put<Item>(`${ItemService.url}/${Item.id}`, Item);
  }
}
