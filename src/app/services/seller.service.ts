import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { URLHelper } from '../helpers/url';
import { Seller } from '../models/seller.model';


@Injectable({
  providedIn: 'root'
})
export class SellerService {
  private static url = `${URLHelper.url}sellers`;

  constructor(private http: HttpClient) {}

  getSellers(): Observable<Seller[]> {
    return this.http.get<Seller[]>(`${SellerService.url}`);
  }

  getSellersById(id: number): Observable<Seller> {
    return this.http.get<Seller>(`${SellerService.url}/${id}`);
  }

  createSeller(Seller: Seller): Observable<Seller> {
    return this.http.post<Seller>(`${SellerService.url}`, Seller);
  }

  updateSeller(Seller: Seller): Observable<Seller> {
    return this.http.put<Seller>(
      `${SellerService.url}/${Seller.id}`,
      Seller
    );
  }
}
