import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Category } from '../models/category.model';
import { URLHelper } from '../helpers/url';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private static url = `${URLHelper.url}categories`;

  constructor(private http: HttpClient) {}

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(`${CategoryService.url}`);
  }

  getCategoriesWithSubcategories(): Observable<Category[]> {
    return this.http.get<Category[]>(`${CategoryService.url}/ws`);
  }

  getCategoriesById(id: number): Observable<Category> {
    return this.http.get<Category>(`${CategoryService.url}/${id}`);
  }

  createCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(`${CategoryService.url}`, category);
  }

  updateCategory(category: Category): Observable<Category> {
    return this.http.put<Category>(
      `${CategoryService.url}/${category.id}`,
      category
    );
  }
}
