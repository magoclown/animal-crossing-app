import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { URLHelper } from '../helpers/url';
import { Fashion } from '../models/fashion.model';



@Injectable({
  providedIn: 'root'
})
export class FashionService {
  private static url = `${URLHelper.url}fashions`;

  constructor(private http: HttpClient) {}

  getFashions(): Observable<Fashion[]> {
    return this.http.get<Fashion[]>(`${FashionService.url}`);
  }

  getFashionsById(id: number): Observable<Fashion> {
    return this.http.get<Fashion>(`${FashionService.url}/${id}`);
  }

  createFashion(Fashion: Fashion): Observable<Fashion> {
    return this.http.post<Fashion>(`${FashionService.url}`, Fashion);
  }

  updateFashion(Fashion: Fashion): Observable<Fashion> {
    return this.http.put<Fashion>(
      `${FashionService.url}/${Fashion.id}`,
      Fashion
    );
  }
}
