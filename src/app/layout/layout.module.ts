import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule, NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SearchComponent } from './search/search.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { ItemComponent } from './item/item.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ItemFormComponent } from './forms/item-form/item-form.component';
import { SelectComponent } from './select/select.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SearchComponent,
    PaginatorComponent,
    ItemComponent,
    DropdownComponent,
    ItemFormComponent,
    SelectComponent,
  ],
  imports: [CommonModule, NgbModule, FormsModule],
  providers: [NgbDropdown],
  exports: [
    HeaderComponent,
    FooterComponent,
    SearchComponent,
    PaginatorComponent,
    ItemComponent,
    DropdownComponent,
    ItemFormComponent,
    SelectComponent,
  ],
})
export class LayoutModule {}
