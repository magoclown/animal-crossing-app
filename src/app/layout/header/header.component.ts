import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/category.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  collapsed = true;

  categories: Category[];

  constructor(private categoryService: CategoryService) {
    this.categoryService.getCategoriesWithSubcategories().subscribe((categories) => {
      this.categories = categories;
    });
  }

  ngOnInit(): void {
  }

}
