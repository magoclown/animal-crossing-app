import { Component, OnInit } from '@angular/core';

import * as S3 from 'aws-sdk/clients/s3';

import { UploadS3Service } from 'src/app/services/upload-s3.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { Subcategory } from 'src/app/models/subcategory.model';
import { Seller } from 'src/app/models/seller.model';
import { Fashion } from 'src/app/models/fashion.model';
import { SellerService } from 'src/app/services/seller.service';
import { FashionService } from 'src/app/services/fashion.service';
import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss'],
})
export class ItemFormComponent implements OnInit {
  selectedFiles: FileList;

  imageSrc: string | ArrayBuffer;

  subcategories: Subcategory[];
  sellers: Seller[];
  fashions: Fashion[];

  item: Item;

  constructor(
    private uploadService: UploadS3Service,
    private subcategoryService: SubcategoryService,
    private sellerService: SellerService,
    private fashionService: FashionService,
    private itemService: ItemService
  ) {
    this.item = new Item();
    this.subcategoryService
      .getSubcategories()
      .subscribe((subcategories: Subcategory[]) => {
        this.subcategories = subcategories;
      });
    this.sellerService.getSellers().subscribe((sellers: Seller[]) => {
      this.sellers = sellers;
    });
    this.fashionService.getFashions().subscribe((fashions: Fashion[]) => {
      this.fashions = fashions;
    });
  }

  ngOnInit(): void {}

  upload() {
    const file = this.selectedFiles.item(0);
    this.uploadService
      .uploadFile(file, this.item.subcategory.name)
      .then((data: S3.ManagedUpload.SendData) => {
        this.item.image = data.Location;
        console.info(this.item);
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        this.itemService.createItem(this.item).subscribe((data) => {
          console.log(data);
        });
      });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;

    const reader = new FileReader();
    reader.onload = (e) => {
      this.imageSrc = reader.result;
    };

    reader.readAsDataURL(this.selectedFiles[0]);
  }

  changeCategory(event) {
    this.item.subcategory = this.subcategories[event - 1];
    console.log(this.item.subcategory);
  }

  changeSeller(event) {
    this.item.seller = this.sellers[event - 1];
  }

  changeFashion(event) {
    this.item.fashion = this.fashions[event - 1];
  }

  autoCalculateSells() {
    this.item.sellsPrice = Math.floor(this.item.purchasePrice * 0.25);
  }
}
