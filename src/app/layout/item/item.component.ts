import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { Item } from 'src/app/models/item.model';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit, OnChanges {
  @Input()
  item: Item;

  currency: string;

  sellsText: string;
  purchaseText: string;

  locale = 'en-US';

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.item) {
      this.currency = this.item.subcategory.id === 15 ? 'M' : 'B';
      this.sellsText =
        this.item.sellsPrice > 0
          ? `${formatNumber(this.item.sellsPrice, this.locale)} B`
          : 'N/A';
      this.purchaseText =
        this.item.purchasePrice > 0
          ? `${formatNumber(this.item.purchasePrice, this.locale)} ${
              this.currency
            }`
          : 'N/A';
    }
  }
}
