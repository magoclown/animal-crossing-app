import { Component, OnInit, Input } from '@angular/core';

import { Subcategory } from 'src/app/models/subcategory.model';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input()
  public displayText: string;

  @Input()
  public displayItems: Subcategory[];

  constructor() { }

  ngOnInit(): void {
  }

}
