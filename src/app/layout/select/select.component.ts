import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  values = [];

  @Output()
  onChange = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  changeSelect(item) {
    this.onChange.emit(item.value);
  }

}
