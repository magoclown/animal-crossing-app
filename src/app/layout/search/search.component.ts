import { Component, OnInit } from '@angular/core';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { Subcategory } from 'src/app/models/subcategory.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  subcategories: Subcategory[];

  constructor(private subcategoryService: SubcategoryService) {
    this.subcategoryService.getSubcategories().subscribe((subcategories) => {
      this.subcategories = subcategories;
    });
  }

  ngOnInit(): void {
  }

}
