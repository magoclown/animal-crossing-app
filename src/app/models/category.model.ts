import { Subcategory } from './subcategory.model';

export class Category {
  public id: number;
  public name: string;
  public subcategories: Subcategory[];
}
