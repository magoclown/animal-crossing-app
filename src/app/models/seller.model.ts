import { Item } from './item.model';

export class Seller {
  id: number;
  name: string;
  tems: Item[];
}
