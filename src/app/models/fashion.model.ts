import { Item } from './item.model';

export class Fashion {
  id: number;
  name: string;
  item: Item[];
}
