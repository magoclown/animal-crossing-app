import { Category } from './category.model';
import { Item } from './item.model';

export class Subcategory {
  id: number;
  name: string;
  category: Category;
  items: Item[];
}
