import { Subcategory } from './subcategory.model';
import { Seller } from './seller.model';
import { Fashion } from './fashion.model';

export class Item {
  id: number;
  name: string;
  image: string;
  purchasePrice: number;
  sellsPrice: number;
  subcategory: Subcategory;
  seller: Seller;
  fashion: Fashion;
}
